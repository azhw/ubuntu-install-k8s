#!/bin/bash

# 部署服务器系统初始化内容

# deploy机器升级软件库
apt-get update && apt-get upgrade -y && apt-get dist-upgrade -y

# deploy机器检测python环境
# python -V &>/dev/null
# if [ $? -ne 0 ];then
#     if cat /etc/redhat-release &>/dev/null;then
#         yum install gcc openssl-devel bzip2-devel 
#         wget https://www.python.org/ftp/python/2.7.16/Python-2.7.16.tgz
#         tar xzf Python-2.7.16.tgz
#         cd Python-2.7.16
#         ./configure --enable-optimizations
#         make altinstall
#         ln -s /usr/bin/python2.7 /usr/bin/python
#         cd -
#     else
#         apt-get install -y python2.7 && ln -s /usr/bin/python2.7 /usr/bin/python
#     fi
# fi

apt-get install -y python3 python3-pip


# deploy机器安装相应软件包
# which python || ln -svf `which python2.7` /usr/bin/python

# if cat /etc/redhat-release &>/dev/null;then
#     yum install git epel-release python-pip sshpass -y
#     [ -f ./get-pip.py ] && python ./get-pip.py || {
#     wget https://bootstrap.pypa.io/pip/2.7/get-pip.py && python get-pip.py
#     }
# else
#     if grep -Ew '20.04|22.04' /etc/issue &>/dev/null;then apt-get install sshpass -y;else apt-get install python-pip sshpass -y;fi
#     [ -f ./get-pip.py ] && python ./get-pip.py || {
#     wget https://bootstrap.pypa.io/pip/2.7/get-pip.py && python get-pip.py
#     }
# fi
# python -m pip install --upgrade "pip < 21.0"

# which pip || ln -svf `which pip` /usr/bin/pip


# deploy机器设置pip安装加速源
if [[ $RUN_ENV == "cn" ]]; then
mkdir -p ~/.pip
cat > ~/.pip/pip.conf <<CB
[global]
index-url = https://mirrors.aliyun.com/pypi/simple
[install]
trusted-host=mirrors.aliyun.com
CB
fi
pip -V
pip install setuptools -U
pip install --no-cache-dir ansible netaddr