#!/bin/bash

# 所有工作节点， 包括master节点和worker节点都需要执行的初始化内容

# 前置要求， 开启root用户登录， 下面的命令全部都直接使用root用户执行
# 前置要求， 开启root用户登录
# 前置要求， 开启root用户登录

# 执行命令遇到错误就退出
set -e
# 脚本中遇到不存在的变量就退出
set -u
# 执行指令的时候，同时把指令输出，方便观察结果
set -x
# 执行管道的时候，如果前面的命令出错，管道后面的命令会停止
set -o pipefail
cp /etc/apt/sources.list /etc/apt/sources.list.bak 
sed -i "s@http://.*archive.ubuntu.com@https://mirrors.tuna.tsinghua.edu.cn@g" /etc/apt/sources.list
sed -i "s@http://.*security.ubuntu.com@https://mirrors.tuna.tsinghua.edu.cn@g" /etc/apt/sources.list
apt update
sed -i 's/#$nrconf{restart} = '"'"'i'"'"';/$nrconf{restart} = '"'"'a'"'"';/g' /etc/needrestart/needrestart.conf
apt full-upgrade -y
apt install -y net-tools telnet curl wget unzip gcc swig automake make perl cpio git \
    libmbedtls-dev libudns-dev libev-dev python3-pip lrzsz iftop nethogs nload htop ifstat iotop vim



apt clean all

source /tmp/config.sh

ip_effective=0
declare curr_node_ip
declare curr_node_hostname

# 主节点遍历 设置主机名 并 修改 /etc/hosts
for i in "${!NODE_LIST[@]}"; do
    item="${NODE_LIST[$i]}"
    item_ip=`echo $item | awk -F ':' '{print $1}'`
    item_hostname=`echo $item | awk -F ':' '{print $2}'`
    if [ $item_ip == $ip ];then
        echo "开始设置主机名称"
        hostnamectl set-hostname $item_hostname
        ip_effective=1
        curr_node_ip=$item_ip
        curr_node_hostname=$item_hostname
    fi

    if [ ! -z "$(cat /etc/hosts | grep $item_ip)" ];then
        echo "$item_ip 已存在"
    else
        echo "$item_ip   $item_hostname" >> /etc/hosts
    fi
done


if [ $ip_effective == 0 ];then
    echo "当前机器IP不在配置的节点中，请检查配置"
    exit 1
fi


# 关闭防火墙
systemctl stop ufw
systemctl disable ufw


# 关闭swap
swapoff -a
sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab


cat > /etc/profile.d/common_config.sh << EOF
# 设置最大保存1000条历史命令
HISTSIZE=10000
# 设置历史命令记录时间格式
HISTTIMEFORMAT="%F %T \$(whoami) "

alias l='ls -AFhlt --color=auto'
alias lh='l | head'
alias ll='ls -l --color=auto'
alias ls='ls --color=auto'
alias vi=vim

GREP_OPTIONS="--color=auto"
alias grep='grep --color'
alias egrep='egrep --color'
alias fgrep='fgrep --color'
EOF

# 设置 vim 语法高亮
sed -i 's@^"syntax on@syntax on@' /etc/vim/vimrc

# PS1
[ -z "$(grep ^PS1 ~/.bashrc)" ] && echo "PS1='\${debian_chroot:+(\$debian_chroot)}\\[\\e[1;32m\\]\\u@\\h\\[\\033[00m\\]:\\[\\033[01;34m\\]\\w\\[\\033[00m\\]\\$ '" >> ~/.bashrc

# history
[ -z "$(grep history-timestamp ~/.bashrc)" ] && echo "PROMPT_COMMAND='{ msg=\$(history 1 | { read x y; echo \$y; });user=\$(whoami); echo \$(date \"+%Y-%m-%d %H:%M:%S\"):\$user:\`pwd\`/:\$msg ---- \$(who am i); } >> /tmp/\`hostname\`.\`whoami\`.history-timestamp'" >> ~/.bashrc


# /etc/security/limits.conf
[ -e /etc/security/limits.d/*nproc.conf ] && rename nproc.conf nproc.conf_bk /etc/security/limits.d/*nproc.conf
[ -z "$(grep 'session required pam_limits.so' /etc/pam.d/common-session)" ] && echo "session required pam_limits.so" >> /etc/pam.d/common-session
sed -i '/^# End of file/,$d' /etc/security/limits.conf
cat >> /etc/security/limits.conf <<EOF
# End of file
* soft nproc 1000000
* hard nproc 1000000
* soft nofile 1000000
* hard nofile 1000000
root soft nproc 1000000
root hard nproc 1000000
root soft nofile 1000000
root hard nofile 1000000
EOF


# 设置时区
timedatectl set-timezone Asia/Shanghai

apt install ntpdate -y
ntpdate time1.aliyun.com

# 添加crontab 定时任务 每一个小时执行一次 ntpdate time1.aliyun.com
if [ ! -f "/etc/crontab" ]; then
    touch /etc/crontab
else
    echo "文件已存在"
fi

if [ ! -z "$(cat /etc/crontab | grep \"time1.aliyun.com\")" ];then
    echo "0 */1 * * * /usr/sbin/ntpdate time1.aliyun.com" >> /etc/crontab
else
    echo "任务已存在"
fi


# /etc/sysctl.conf
[ -z "$(grep 'fs.file-max' /etc/sysctl.conf)" ] && cat >> /etc/sysctl.conf << EOF

# 这个参数定义了系统中最大的文件句柄数。文件句柄是用于访问文件的数据结构。增加这个值可以提高系统同时打开文件的能力。
fs.file-max = 1000000

# inotify是Linux内核中的一个机制，用于监视文件系统事件。这个参数定义了每个用户可以创建的inotify实例的最大数量。
fs.inotify.max_user_instances = 8192

# 当系统遭受SYN洪水攻击时，启用syncookies可以防止系统资源被耗尽。SYN cookies是一种机制，用于在TCP三次握手中保护服务器端资源。
net.ipv4.tcp_syncookies = 1

# 这个参数定义了TCP连接中，等待关闭的时间。当一端发送FIN信号后，等待对端关闭连接的超时时间。
net.ipv4.tcp_fin_timeout = 30

# 启用该参数后，可以允许将TIME-WAIT状态的TCP连接重新用于新的连接。这可以减少系统中TIME-WAIT连接的数量。
net.ipv4.tcp_tw_reuse = 1

# 这个参数定义了本地端口的范围，用于分配给发送请求的应用程序。它限制了可用于客户端连接的本地端口范围。
net.ipv4.ip_local_port_range = 1024 65000

# 这个参数定义了TCP连接请求的队列长度。当系统处理不及时时，超过该队列长度的连接请求将被拒绝。
net.ipv4.tcp_max_syn_backlog = 16384

# 这个参数定义了系统同时保持TIME-WAIT状态的最大数量。超过这个数量的连接将被立即关闭。
net.ipv4.tcp_max_tw_buckets = 6000

# 这个参数定义了内核路由表清理的时间间隔，单位是秒。它影响路由缓存的生命周期。
net.ipv4.route.gc_timeout = 100

# 这个参数定义了在发送SYN请求后，等待对端回应的次数。超过指定次数后仍未响应，连接将被认为失败。
net.ipv4.tcp_syn_retries = 1

# 这个参数定义了在发送SYN+ACK回应后，等待对端发送ACK的次数。超过指定次数后仍未收到ACK，连接将被认为失败。
net.ipv4.tcp_synack_retries = 1

# 这个参数定义了监听队列的最大长度。当服务器正在处理的连接数超过此值时，新的连接请求将被拒绝。
net.core.somaxconn = 32768

# 这个参数定义了网络设备接收队列的最大长度。当接收队列已满时，新的数据包将被丢弃。
net.core.netdev_max_backlog = 32768

# 这个参数定义了每个网络设备接收队列在每个时间间隔中可以处理的数据包数量。
net.core.netdev_budget = 5000

# 禁用TCP时间戳。时间戳可以用于解决网络中的数据包乱序问题，但在高负载环境下可能会增加开销。
net.ipv4.tcp_timestamps = 0

# 这个参数定义了系统中允许存在的最大孤立（没有关联的父连接）TCP连接数量。超过这个数量的孤立连接将被立即关闭。
net.ipv4.tcp_max_orphans = 32768
EOF
sysctl -p



# Normal display of Chinese in the text
# apt-get -y install locales

# echo 'export LANG=en_US.UTF-8'|tee -a /etc/profile && source /etc/profile

# sed -i 's@^ACTIVE_CONSOLES.*@ACTIVE_CONSOLES="/dev/tty[1-2]"@' /etc/default/console-setup
# #sed -i 's@^@#@g' /etc/init/tty[3-6].conf
# locale-gen en_US.UTF-8
# echo "en_US.UTF-8 UTF-8" > /var/lib/locales/supported.d/local
# cat > /etc/default/locale << EOF
# LANG=en_US.UTF-8
# LANGUAGE=en_US:en
# EOF
#sed -i 's@^@#@g' /etc/init/control-alt-delete.conf
