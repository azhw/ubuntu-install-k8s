#!/bin/bash
# auther: boge
# descriptions:  the shell scripts will use ansible to deploy K8S at binary for siample
# docker-tag
# curl -s -S "https://registry.hub.docker.com/v2/repositories/easzlab/kubeasz-k8s-bin/tags/" | jq '."results"[]["name"]' |sort -rn
# github:   https://github.com/easzlab/kubeasz
#########################################################################
# 此脚本安装过的操作系统 CentOS/RedHat 7, Ubuntu 16.04/18.04/20.04/22.04
#########################################################################

# 前置要求， 开启root用户登录， 下面的命令全部都直接使用root用户执行
# 前置要求， 开启root用户登录
# 前置要求， 开启root用户登录

# 导入配置
source ./config.sh
if [[ $DEBUG == 1 ]]; then
    # 脚本只要发生错误，就终止执行
    set -e
    # 脚本中遇到不存在的变量就退出
    set -u
    # 执行指令的时候，同时把指令输出，方便观察结果
    set -x
    # 执行管道的时候，如果前面的命令出错，管道后面的命令会停止
    set -o pipefail
fi

source ./deploy_init_sys.sh


# 在deploy机器做其他node的ssh免密操作
for i in "${!ALL_NODE_IP_LIST[@]}"; do
    node_ip="${ALL_NODE_IP_LIST[$i]}"
    echo "============ ${node_ip} ===========";

    # 所有节点初始化系统
    scp ./work_node_init_sys.sh ${USER}@${node_ip}:/tmp/work_node_init_sys.sh
    scp ./config.sh ${USER}@${node_ip}:/tmp/config.sh
    ssh -o StrictHostKeyChecking=no ${USER}@${node_ip} "bash /tmp/work_node_init_sys.sh"

done

#确认所有节点执行初始化结果

for i in "${!ALL_NODE_IP_LIST[@]}"; do
    node_ip="${ALL_NODE_IP_LIST[$i]}"
    ssh -o StrictHostKeyChecking=no ${USER}@${node_ip} "hostname"
done
echo "确认所有节点主机名称"
read -p "" xxxxxx

# deploy机器下载k8s二进制安装脚本(注：这里下载可能会因网络原因失败，可以多尝试运行该脚本几次)

# 获取软件包
software_packet=""
if ls -1v ./kubeasz*.tar.gz &>/dev/null;then 
    software_packet="$(ls -1v ./kubeasz*.tar.gz )"
fi
echo $software_packet



if [[ ${software_packet} == '' ]];then
    if [[ ! -f ./ezdown ]];then
        curl -C- -fLO --retry 3 https://github.com/easzlab/kubeasz/releases/download/${release}/ezdown
    fi
    # 使用工具脚本下载
    sed -ri "s+^(K8S_BIN_VER=).*$+\1${k8s_version}+g" ezdown
    chmod +x ./ezdown
    # ubuntu_22         to download package of Ubuntu 22.04
    ./ezdown -D && ./ezdown -P ubuntu_22 && ./ezdown -X
else
    tar xvf ${software_packet} -C /etc/
    sed -ri "s+^(K8S_BIN_VER=).*$+\1${k8s_version}+g" ${PWD}/ezdown
    chmod +x ${PWD}/{ezctl,ezdown}
    chmod +x ./ezdown
    ./ezdown -D  # 离线安装 docker，检查本地文件，正常会提示所有文件已经下载完成，并上传到本地私有镜像仓库
    ./ezdown -S  # 启动 kubeasz 容器
fi


# 初始化一个名为$CLUSTER_NAME的k8s集群配置
rm -rf /etc/kubeasz/clusters/${CLUSTER_NAME}
${PWD}/ezctl new ${CLUSTER_NAME}
if [[ $? -ne 0 ]];then
    echo "cluster name [${CLUSTER_NAME}] was exist in ${PWD}/clusters/${CLUSTER_NAME}."
    exit 1
fi


if [[ ${software_packet} != '' ]];then
    # 设置参数，启用离线安装
    # 离线安装文档：https://github.com/easzlab/kubeasz/blob/3.6.2/docs/setup/offline_install.md
    sed -i 's/^INSTALL_SOURCE.*$/INSTALL_SOURCE: "offline"/g' ${PWD}/clusters/${CLUSTER_NAME}/config.yml
fi


# to check ansible service
ansible all -m ping

#---------------------------------------------------------------------------------------------------


#修改二进制安装脚本配置 config.yml
sed -ri "s+^(CLUSTER_NAME:).*$+\1 \"${CLUSTER_NAME}\"+g" ${PWD}/clusters/${CLUSTER_NAME}/config.yml

## k8s上日志及容器数据存独立磁盘步骤（参考阿里云的）
mkdir -p /var/lib/container/{kubelet,docker,nfs_dir} /var/lib/{kubelet,docker} /nfs_dir

## 不用fdisk分区，直接格式化数据盘 mkfs.ext4 /dev/vdb，按下面添加到fstab后，再mount -a刷新挂载(blkid /dev/sdx)
## cat /etc/fstab     
# UUID=105fa8ff-bacd-491f-a6d0-f99865afc3d6 /                       ext4    defaults        1 1
# /dev/vdb /var/lib/container/ ext4 defaults 0 0
# /var/lib/container/kubelet /var/lib/kubelet none defaults,bind 0 0
# /var/lib/container/docker /var/lib/docker none defaults,bind 0 0
# /var/lib/container/nfs_dir /nfs_dir none defaults,bind 0 0

## tree -L 1 /var/lib/container
# /var/lib/container
# ├── docker
# ├── kubelet
# └── lost+found

# docker data dir
DOCKER_STORAGE_DIR="/var/lib/container/docker"
sed -ri "s+^(STORAGE_DIR:).*$+STORAGE_DIR: \"${DOCKER_STORAGE_DIR}\"+g" ${PWD}/clusters/${CLUSTER_NAME}/config.yml
# containerd data dir
CONTAINERD_STORAGE_DIR="/var/lib/container/containerd"
sed -ri "s+^(STORAGE_DIR:).*$+STORAGE_DIR: \"${CONTAINERD_STORAGE_DIR}\"+g" ${PWD}/clusters/${CLUSTER_NAME}/config.yml
# kubelet logs dir
KUBELET_ROOT_DIR="/var/lib/container/kubelet"
sed -ri "s+^(KUBELET_ROOT_DIR:).*$+KUBELET_ROOT_DIR: \"${KUBELET_ROOT_DIR}\"+g" ${PWD}/clusters/${CLUSTER_NAME}/config.yml
if [[ $CLUSTER_NAME != 'aws' ]]; then
    # docker aliyun repo
    REG_MIRRORS="https://pqbap4ya.mirror.aliyuncs.com"
    sed -ri "s+^REG_MIRRORS:.*$+REG_MIRRORS: \'[\"${REG_MIRRORS}\"]\'+g" ${PWD}/clusters/${CLUSTER_NAME}/config.yml
fi
# [docker]信任的HTTP仓库
sed -ri "s+127.0.0.1/8+${NET_NUM}.0/24+g" ${PWD}/clusters/${CLUSTER_NAME}/config.yml
# disable dashboard auto install
sed -ri "s+^(dashboard_install:).*$+\1 \"no\"+g" ${PWD}/clusters/${CLUSTER_NAME}/config.yml


# 融合配置准备(按示例部署命令这里会生成testk8s.boge.com这个域名，部署脚本会基于这个域名签证书，优势是后面访问kube-apiserver，可以基于此域名解析任意IP来访问，灵活性更高)
CLUSEER_WEBSITE="${CLUSTER_NAME}k8s.${DOMAIN_NAME}"
lb_num=$(grep -wn '^MASTER_CERT_HOSTS:' ${PWD}/clusters/${CLUSTER_NAME}/config.yml |awk -F: '{print $1}')
lb_num1=$(expr ${lb_num} + 1)
lb_num2=$(expr ${lb_num} + 2)
sed -ri "${lb_num1}s+.*$+  - "${CLUSEER_WEBSITE}"+g" ${PWD}/clusters/${CLUSTER_NAME}/config.yml
sed -ri "${lb_num2}s+(.*)$+#\1+g" ${PWD}/clusters/${CLUSTER_NAME}/config.yml

# node节点最大pod 数
MAX_PODS="120"
sed -ri "s+^(MAX_PODS:).*$+\1 ${MAX_PODS}+g" ${PWD}/clusters/${CLUSTER_NAME}/config.yml

# calico 自建机房都在二层网络可以设置 CALICO_IPV4POOL_IPIP=“off”,以提高网络性能; 公有云上VPC在三层网络，需设置CALICO_IPV4POOL_IPIP: "Always"开启ipip隧道
#sed -ri "s+^(CALICO_IPV4POOL_IPIP:).*$+\1 \"off\"+g" ${PWD}/clusters/${CLUSTER_NAME}/config.yml

# 修改二进制安装脚本配置 hosts
# clean old ip
sed -ri '/192.168.1.1/d' ${PWD}/clusters/${CLUSTER_NAME}/hosts
sed -ri '/192.168.1.2/d' ${PWD}/clusters/${CLUSTER_NAME}/hosts
sed -ri '/192.168.1.3/d' ${PWD}/clusters/${CLUSTER_NAME}/hosts
sed -ri '/192.168.1.4/d' ${PWD}/clusters/${CLUSTER_NAME}/hosts
sed -ri '/192.168.1.5/d' ${PWD}/clusters/${CLUSTER_NAME}/hosts

# 输入准备创建ETCD集群的主机位
for i in "${!ETCD_LIST[@]}"; do
    item_ip="${ETCD_LIST[$i]}"
    sed -i "/\[etcd/a $item_ip"  ${PWD}/clusters/${CLUSTER_NAME}/hosts
done

# 输入准备创建KUBE-MASTER集群的主机位
for i in "${!MASTER_NODE_IP_LIST[@]}"; do
    item_ip="${MASTER_NODE_IP_LIST[$i]}"
    sed -i "/\[kube_master/a $item_ip"  ${PWD}/clusters/${CLUSTER_NAME}/hosts
done

# 输入准备创建KUBE-NODE集群的主机位
for i in "${!WORKER_NODE_IP_LIST[@]}"; do
    item_ip="${WORKER_NODE_IP_LIST[$i]}"
    sed -i "/\[kube_node/a $item_ip"  ${PWD}/clusters/${CLUSTER_NAME}/hosts
done

# 配置容器运行时CNI
case ${cni} in
    flannel)
    sed -ri "s+^CLUSTER_NETWORK=.*$+CLUSTER_NETWORK=\"${cni}\"+g" ${PWD}/clusters/${CLUSTER_NAME}/hosts
    ;;
    calico)
    sed -ri "s+^CLUSTER_NETWORK=.*$+CLUSTER_NETWORK=\"${cni}\"+g" ${PWD}/clusters/${CLUSTER_NAME}/hosts
    ;;
    cilium)
    sed -ri "s+^CLUSTER_NETWORK=.*$+CLUSTER_NETWORK=\"${cni}\"+g" ${PWD}/clusters/${CLUSTER_NAME}/hosts
    ;;
    *)
    echo "cni need be flannel or calico or cilium."
    exit 11
esac

# 配置K8S的ETCD数据备份的定时任务
#  https://github.com/easzlab/kubeasz/blob/master/docs/op/cluster_restore.md
if cat /etc/redhat-release &>/dev/null;then
    if ! grep -w '94.backup.yml' /var/spool/cron/root &>/dev/null;then echo "00 00 * * * /usr/local/bin/ansible-playbook -i /etc/kubeasz/clusters/${CLUSTER_NAME}/hosts -e @/etc/kubeasz/clusters/${CLUSTER_NAME}/config.yml /etc/kubeasz/playbooks/94.backup.yml &> /dev/null; find /etc/kubeasz/clusters/${CLUSTER_NAME}/backup/ -type f -name '*.db' -mtime +3|xargs rm -f" >> /var/spool/cron/root;else echo exists ;fi
    chown root.crontab /var/spool/cron/root
    chmod 600 /var/spool/cron/root
    rm -f /var/run/cron.reboot
    service crond restart
else
    if ! grep -w '94.backup.yml' /var/spool/cron/crontabs/root &>/dev/null;then echo "00 00 * * * /usr/local/bin/ansible-playbook -i /etc/kubeasz/clusters/${CLUSTER_NAME}/hosts -e @/etc/kubeasz/clusters/${CLUSTER_NAME}/config.yml /etc/kubeasz/playbooks/94.backup.yml &> /dev/null; find /etc/kubeasz/clusters/${CLUSTER_NAME}/backup/ -type f -name '*.db' -mtime +3|xargs rm -f" >> /var/spool/cron/crontabs/root;else echo exists ;fi
    chown root.crontab /var/spool/cron/crontabs/root
    chmod 600 /var/spool/cron/crontabs/root
    rm -f /var/run/crond.reboot
    service cron restart
fi





#---------------------------------------------------------------------------------------------------
# 准备开始安装了
rm -rf ${PWD}/{dockerfiles,docs,.gitignore,pics,dockerfiles} &&\
find ${PWD}/ -name '*.md'|xargs rm -f

# 输出所有节点主机名称
read -p "Enter to continue deploy k8s to all nodes >>>" YesNobbb

# now start deploy k8s cluster 
cd ${PWD}/

# to prepare CA/certs & kubeconfig & other system settings 
${PWD}/ezctl setup ${CLUSTER_NAME} 01
sleep 1
# to setup the etcd cluster
${PWD}/ezctl setup ${CLUSTER_NAME} 02
sleep 1
# to setup the container runtime(docker or containerd)
case ${cri} in
    containerd)
    sed -ri "s+^CONTAINER_RUNTIME=.*$+CONTAINER_RUNTIME=\"${cri}\"+g" ${PWD}/clusters/${CLUSTER_NAME}/hosts
    ${PWD}/ezctl setup ${CLUSTER_NAME} 03
    ;;
    docker)
    sed -ri "s+^CONTAINER_RUNTIME=.*$+CONTAINER_RUNTIME=\"${cri}\"+g" ${PWD}/clusters/${CLUSTER_NAME}/hosts
    ${PWD}/ezctl setup ${CLUSTER_NAME} 03
    ;;
    *)
    echo "cri need be containerd or docker."
    exit 11
esac
sleep 1
# to setup the master nodes
${PWD}/ezctl setup ${CLUSTER_NAME} 04
sleep 1
# to setup the worker nodes
${PWD}/ezctl setup ${CLUSTER_NAME} 05
sleep 1
# to setup the network plugin(flannel、calico...)
${PWD}/ezctl setup ${CLUSTER_NAME} 06
sleep 1
# to setup other useful plugins(metrics-server、coredns...)
${PWD}/ezctl setup ${CLUSTER_NAME} 07
sleep 1
# [可选]对集群所有节点进行操作系统层面的安全加固  https://github.com/dev-sec/ansible-os-hardening
#ansible-playbook roles/os-harden/os-harden.yml
#sleep 1
#cd `dirname ${software_packet:-/tmp}`


k8s_bin_path='/opt/kube/bin'


echo "-------------------------  k8s version list  ---------------------------"
${k8s_bin_path}/kubectl version
echo
echo "-------------------------  All Healthy status check  -------------------"
${k8s_bin_path}/kubectl get componentstatus
echo
echo "-------------------------  k8s cluster info list  ----------------------"
${k8s_bin_path}/kubectl cluster-info
echo
echo "-------------------------  k8s all nodes list  -------------------------"
${k8s_bin_path}/kubectl get node -o wide
echo
echo "-------------------------  k8s all-namespaces's pods list   ------------"
${k8s_bin_path}/kubectl get pod --all-namespaces
echo
echo "-------------------------  k8s all-namespaces's service network   ------"
${k8s_bin_path}/kubectl get svc --all-namespaces
echo
echo "-------------------------  k8s welcome for you   -----------------------"
echo

# you can use k alias kubectl to siample
echo "alias k=kubectl && complete -F __start_kubectl k" >> ~/.bashrc

# get dashboard url
${k8s_bin_path}/kubectl cluster-info|grep dashboard|awk '{print $NF}'|tee -a /root/k8s_results

# get login token
${k8s_bin_path}/kubectl -n kube-system describe secret $(${k8s_bin_path}/kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')|grep 'token:'|awk '{print $NF}'|tee -a /root/k8s_results
echo
echo "you can look again dashboard and token info at  >>> /root/k8s_results <<<"
echo ">>>>>>>>>>>>>>>>> You need to excute command [ reboot ] to restart all nodes <<<<<<<<<<<<<<<<<<<<"
#find / -type f -name "kubeasz*.tar.gz" -o -name "k8s_install_new.sh"|xargs rm -f


