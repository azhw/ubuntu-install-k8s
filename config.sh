#!/bin/bash
# 部署服务器配置变量
DEBUG=1
# root用户密码
ROOT_PASSWD=soft01
# 节点列表  ip:主机名称:角色（master|worker）
NODE_LIST=(
"192.168.56.11:k8s-master01:master"
"192.168.56.12:k8s-master02:master"
"192.168.56.13:k8s-worker01:worker"
"192.168.56.14:k8s-worker02:worker"
)
ETCD_LIST=(
"192.168.56.12"
"192.168.56.13"
"192.168.56.14"
)
NET_NUM=192.168.56
# 底座容器类型 [containerd|docker]
cri=containerd
# cni类型 [calico|flannel|cilium]
cni=calico
#  集群域名
DOMAIN_NAME=azhw.cn
# 集群名称 集群名称如果带有cn， 则在安装脚本中默认为国内安装。如果不带cn则默认为国际安装
CLUSTER_NAME=ank8s_cn
# 运行环境 cn表示国内， 但是现在没什么用
RUN_ENV=cn
PWD=/etc/kubeasz

# 变量定义
export release=3.6.2  # 支持k8s多版本使用,定义下面k8s_ver变量版本范围: 1.28.1 v1.27.5 v1.26.8 v1.25.13 v1.24.17
export k8s_version=v1.27.5  # | docker-tag tags easzlab/kubeasz-k8s-bin   注意: k8s 版本 >= 1.24 时，仅支持 containerd
# 定义离线压缩包位置
offline_tar=./kubeasz-ubuntu-22.04.3-v1.27.5.tar.gz

net_work_name="ens33"
#获取当前执行主机ip
ip=$(ip addr | grep ${net_work_name} | grep inet | grep -v inet6 | awk '{print $2}' | cut -d/ -f1)

# 遍历配置中的服务器定义，获取到所有节点的ip和分类
MASTER_NODE_IP_LIST=()
WORKER_NODE_IP_LIST=()
ALL_NODE_IP_LIST=()

for i in "${!NODE_LIST[@]}"; do
    item="${NODE_LIST[$i]}"
    echo "item=${item}"
    item_ip=`echo $item | awk -F ':' '{print $1}'`
    echo "item_ip=${item_ip}"
    item_homename=`echo $item | awk -F ':' '{print $2}'`
    echo "item_homename=${item_homename}"
    item_role=`echo $item | awk -F ':' '{print $3}'`
    echo "item_role=${item_role}"
    ALL_NODE_IP_LIST+=( $item_ip )
    if [[ ${item_role} == 'master' ]];then
       MASTER_NODE_IP_LIST+=( $item_ip )
    fi

   if [[ ${item_role} == 'worker' ]];then
       WORKER_NODE_IP_LIST+=( $item_ip )
   fi
done

# echo "主节点ip列表：" ${MASTER_NODE_IP_LIST[*]}
# echo "工作节点ip列表：" ${WORKER_NODE_IP_LIST[*]}
# echo ${ALL_NODE_IP_LIST[*]}
# echo "确认节点数据"
# read -p "" xxxx