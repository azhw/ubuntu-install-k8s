#!/bin/bash

echo 'PermitRootLogin yes' >> /etc/ssh/sshd_config
echo 'root:boge'|chpasswd
systemctl restart sshd && systemctl status sshd

# 免密登录
ssh-keygen -t rsa -P '' -f /root/.ssh/id_rsa
ssh-copy-id root@192.168.44.11
ssh-copy-id root@192.168.44.12