#!/bin/bash

# set ip and dns
validate_ip() {
    local ip_var_name=$1

    while true; do
        read -p "Input IP address($ip_var_name): " $ip_var_name

        # 检测是否为空
        if [ -z "${!ip_var_name}" ]; then
            echo "Input is empty. Please try again."
            continue
        fi

        # 检测是否符合IP地址的格式
        if ! [[ ${!ip_var_name} =~ ^([0-9]{1,3}\.){3}[0-9]{1,3}$ ]]; then
            echo "Invalid IP address format. Please try again."
            continue
        fi

        # 输入符合要求，跳出循环
        break
    done
}
ip_address=192.168.44.11
# 调用函数并传递变量名作为参数
ip_gateway=192.168.44.2

dns1_ip=223.5.5.5
dns2_ip=114.114.114.114

network_name=ens33


cat > /etc/netplan/00-installer-config.yaml << EOF
network:
  version: 2
  renderer: networkd
  ethernets:
    ${network_name}:
      dhcp4: false
      dhcp6: false
      addresses:
        - ${ip_address}/24
      routes:
        - to: default
          via: ${ip_gateway}
      nameservers:
          addresses: [${dns1_ip}, ${dns2_ip}]
EOF
netplan apply
apt install resolvconf -y

vim /etc/resolvconf/resolv.conf.d/head
nameserver 223.5.5.5
nameserver 114.114.114.114

systemctl restart resolvconf